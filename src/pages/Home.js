// dependencies
import	React from 'react';

// Bootstrap dependencies
import Container from 'react-bootstrap/Container';

// App Components
import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';

export default function Home(){
	return (
			<Container fluid>
			    <Banner /> 
			    <Highlights />      
			</Container>
		)
}