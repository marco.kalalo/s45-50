/*
  after installing, remove all files inside the src folder except index.js
  remove the README.md file in the root folder of the app
*/
// set up/import dependencies
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
/*
  install bootstrap and react bootstrap first: npm install bootstrap@4.6.0 react-bootstrap@1.5.2
*/
import 'bootstrap/dist/css/bootstrap.min.css';

// App Components
/*
  every component has to be imported before it can be rendered inside the index.js
*/
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';

/*
ReactDOM.render()
  responsible for injecting/inserting the whole React.js Project inside the webpage
*/
/*
  depricated version of the render() in react
*/
// React.createElement('h1', null, 'Hello World');

/*
  JSX - JavaScript XML - is an extention of JS that let's us create objects which will then be compiled and added as HTML elements

  With JSX..
    -we are able to create HTML elements using JS
    -we are also able to create JS objects that will then be compuiled and added as HTML elements

*/

/*
  Fragment - used to render different components inside the index.js. without it, the webpage will return errors since, it is the of JS to display two or more components in the frontend
    <>
    </>
      -also accepted in place of the Fragment but not all browsers are able to read this. also this does not support keys or attributes
*/

ReactDOM.render(
  <Fragment>
    <AppNavbar /> 
    <Home />
  </Fragment>,
  document.getElementById('root')
);
